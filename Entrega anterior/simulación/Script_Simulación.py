#Librerias a utilizar
import numpy as np
import matplotlib.pyplot as plt
#Parámetros
#Paso de tiempo
deltat=1
#Definición de tiempo cero
t=0
#Temperatura interna inicial
T=20
#Espesor de paredes 1,2 y 3
L1=0.08
L2=0.14
L3=0.08
#Área de paredes 1,2 y 3
A1=1
A2=1
A3=1
#Masa del aire interno
m=1.29
#Calor especifico del aire
c=721
#Conductividad térmica de los materiales de las paredes
k1=0.157
k2=0.0413
k3=0.157
#Numero de iteraciones
n=86400
#Matrices de las variables de interés
#Matriz de temperatura interna
MT = np.zeros(n)
#Matriz de tiempo
Mt = np.zeros(n)
#Matriz de temperatura ambiente
MTa = np.zeros(n)
#Ecuaciones del fenómeno físico
for i in range (n):
  MT[i]=T
  Mt[i]=t
  #Modelo de temperatura ambiente
  Ta=10+(5*np.sin(2*np.pi*(1/86400)*t))
  #Modelo de corriente para la bomba térmica
  MTa[i]=Ta
  I=0.5*np.sin(2*np.pi*(1/86400)*t)
  C=c*m
  R1=L1/(k1*A1)
  R2=L2/(k2*A2)
  R3=L3/(k3*A3)
  Qp=(T-Ta)/(R1+R2+R3)
  Qc=3*(I)
  Qa=-Qc-Qp
  derT=Qa/C
  T=T+(derT*deltat)
  t=t+deltat
#Gráficas
plt.title('T ambiental senoidal, bomba con un flujo de calor negativo senoidal y mayor grosor de pared')
plt.plot(Mt,MT,color='red')
plt.plot(Mt,MTa,color='blue')
plt.xlabel('Tiempo (s)')
plt.ylabel('Temperatura (°C)')
plt.grid()
plt.legend(['Temperatura interna' , 'Temperatura ambiente'], loc=1)