import Adafruit_DHT
import time
import RPi.GPIO as GPIO
from signal import signal, SIGTERM, SIGHUP, pause
from rpi_lcd import LCD
lcd =LCD()

def safe_exit(signum, frame):
    exit(1)

pin1 = 26
pin3 = 20

GPIO.setmode(GPIO.BCM)
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin1, GPIO.OUT)
GPIO.setup(pin3, GPIO.OUT)

def calor():
	GPIO.output(pin1, GPIO.HIGH)
	GPIO.output(pin3, GPIO.LOW)
	lcd.text("Estado:",1)
	lcd.text("Enfriando",2)
	time.sleep(1)
def frio():
	GPIO.output(pin1, GPIO.LOW)
	GPIO.output(pin3, GPIO.HIGH)
	lcd.text("Estado:",1)
	lcd.text("Calentando",2)
	time.sleep(1)
def estable():
	GPIO.output(pin1, GPIO.LOW)
	GPIO.output(pin3, GPIO.LOW)
	lcd.text("Estado:",1)
	lcd.text("Sin Cambio",2)
	time.sleep(1)

print("INGRESANDO AL LAZO WHILE")

try:
	while True:
		sensor = Adafruit_DHT.DHT22
		pin = 4
		Tref=28
		humedad , temperatura = Adafruit_DHT.read_retry(sensor, pin)
		error = Tref-temperatura
		signal(SIGTERM, safe_exit)
		signal(SIGHUP, safe_exit)
		lcd.text("Temp. Medida:",1)
		lcd.text(f"{temperatura:.4}",2
		time.sleep(1)
		lcd.text("Temp. Referencia:",1)
		lcd.text(f"{Tref:.2}",2)
		time.sleep(1)
		if error>0:
			frio()
        if error<0:
            calor()
        if error == 0:
            estable()
except KeyboardInterrupt:
    # CTRL+C DETECTADO
    print ("SALIENDO DEL LAZO WHILE")
finally:
    lcd.clear() 
