import Adafruit_DHT
import time

print("INGRESANDO AL LAZO WHILE")

try:
	while True:
		sensor = Adafruit_DHT.DHT22
		pin = 4
		humedad , temperatura = Adafruit_DHT.read_retry(sensor, pin)
		print ("TEMPERATURA: ", temperatura)
		time.sleep(1)
except KeyboardInterrupt:
    # CTRL+C DETECTADO
    print ("SALIENDO DEL LAZO WHILE")
