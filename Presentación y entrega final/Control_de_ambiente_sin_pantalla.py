import Adafruit_DHT
import time
import RPi.GPIO as GPIO

pin1 = 26
pin3 = 20

GPIO.setmode(GPIO.BCM)
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin1, GPIO.OUT)
GPIO.setup(pin3, GPIO.OUT)

def calor():
	GPIO.output(pin1, GPIO.HIGH)
	print("Celda Peltier 1: ON")
	GPIO.output(pin3, GPIO.LOW)
	print("Celda Peltier 2: OFF")
	print("Estado: ENFRIANDO")
	time.sleep(1)
def frio():
	GPIO.output(pin1, GPIO.LOW)
	print("Celda Peltier 1: OFF")
	GPIO.output(pin3, GPIO.HIGH)
	print("Celda Peltier 2: ON")
	print("Estado: CALENTANDO")
	time.sleep(1)
def estable():
	GPIO.output(pin1, GPIO.LOW)
	print("Celda Peltier 1: OFF")
	GPIO.output(pin3, GPIO.LOW)
	print("Celda Peltier 2: OFF")
	print("Estado: ESTABLE")
	time.sleep(1)

print("INGRESANDO AL LAZO WHILE")

try:
	while True:
		sensor = Adafruit_DHT.DHT22
		pin = 4
		Tref=28
		humedad , temperatura = Adafruit_DHT.read_retry(sensor, pin)
		print("-------------------------------------")
		print ("| TEMPERATURA: ", temperatura, "|")
		print ("| TEMPERATURA REFERENCIA: ", Tref, "|")
		print("-------------------------------------")
		error = Tref-temperatura
		time.sleep(1)
		if error>0:
			frio()
        if error<0:
            calor()
        if error == 0:
            estable()
except KeyboardInterrupt:
    # CTRL+C DETECTADO
    print ("SALIENDO DEL LAZO WHILE")
